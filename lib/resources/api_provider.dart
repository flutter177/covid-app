import 'package:covid_app/models/vietnam_model.dart';
import 'package:dio/dio.dart';

import '../models/covid_model.dart';

class ApiProvider {
  final Dio _dio = Dio();
  final String _url = 'https://api.covid19api.com/summary';
  final String _url2 =
      'https://api.apify.com/v2/key-value-stores/EaCBL1JNntjR3EakU/records/LATEST?disableRedirect=true';

  Future<CovidModel?> fetchCovidList() async {
    CovidModel? covid;
    try {
      Response response = await _dio.get(_url);
      covid = CovidModel.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
    }

    return covid;
  }

  Future<VietNamModel?> fetchVietNamCovidList() async {
    VietNamModel? covid;
    try {
      Response response = await _dio.get(_url2);
      covid = VietNamModel.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
    }

    return covid;
  }
}
