import 'package:covid_app/models/vietnam_model.dart';

import '../models/covid_model.dart';
import 'api_provider.dart';

class ApiRepository {
  final _provider = ApiProvider();

  Future<CovidModel?> fetchCovidList() {
    return _provider.fetchCovidList();
  }

  Future<VietNamModel?> fetchVietNamCovidList() {
    return _provider.fetchVietNamCovidList();
  }
}

class NetworkError extends Error {}
