// To parse this JSON data, do
//
//     final vietNamModel = vietNamModelFromJson(jsonString);

import 'dart:convert';

VietNamModel vietNamModelFromJson(String str) =>
    VietNamModel.fromJson(json.decode(str));

String vietNamModelToJson(VietNamModel data) => json.encode(data.toJson());

class VietNamModel {
  VietNamModel({
    required this.infected,
    required this.recovered,
    required this.treated,
    required this.died,
    required this.infectedToday,
    required this.recoveredToday,
    required this.treatedToday,
    required this.diedToday,
    required this.overview,
    required this.locations,
    required this.sourceUrl,
    required this.lastUpdatedAtApify,
    required this.readMe,
  });

  int infected;
  int recovered;
  int treated;
  int died;
  int infectedToday;
  int recoveredToday;
  int treatedToday;
  int diedToday;
  List<Overview> overview;
  List<Location> locations;
  String sourceUrl;
  DateTime lastUpdatedAtApify;
  String readMe;

  factory VietNamModel.fromJson(Map<String, dynamic> json) => VietNamModel(
        infected: json["infected"],
        recovered: json["recovered"],
        treated: json["treated"],
        died: json["died"],
        infectedToday: json["infectedToday"],
        recoveredToday: json["recoveredToday"],
        treatedToday: json["treatedToday"],
        diedToday: json["diedToday"],
        overview: List<Overview>.from(
            json["overview"].map((x) => Overview.fromJson(x))),
        locations: List<Location>.from(
            json["locations"].map((x) => Location.fromJson(x))),
        sourceUrl: json["sourceUrl"],
        lastUpdatedAtApify: DateTime.parse(json["lastUpdatedAtApify"]),
        readMe: json["readMe"],
      );

  Map<String, dynamic> toJson() => {
        "infected": infected,
        "recovered": recovered,
        "treated": treated,
        "died": died,
        "infectedToday": infectedToday,
        "recoveredToday": recoveredToday,
        "treatedToday": treatedToday,
        "diedToday": diedToday,
        "overview": List<dynamic>.from(overview.map((x) => x.toJson())),
        "locations": List<dynamic>.from(locations.map((x) => x.toJson())),
        "sourceUrl": sourceUrl,
        "lastUpdatedAtApify": lastUpdatedAtApify.toIso8601String(),
        "readMe": readMe,
      };
}

class Location {
  Location({
    required this.name,
    required this.death,
    required this.treating,
    required this.cases,
    required this.recovered,
    required this.casesToday,
  });

  String name;
  int death;
  int treating;
  int cases;
  int recovered;
  int casesToday;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        name: json["name"],
        death: json["death"],
        treating: json["treating"],
        cases: json["cases"],
        recovered: json["recovered"],
        casesToday: json["casesToday"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "death": death,
        "treating": treating,
        "cases": cases,
        "recovered": recovered,
        "casesToday": casesToday,
      };
}

class Overview {
  Overview({
    required this.date,
    required this.death,
    required this.treating,
    required this.cases,
    required this.recovered,
    required this.avgCases7Day,
    required this.avgRecovered7Day,
    required this.avgDeath7Day,
  });

  String date;
  int death;
  int treating;
  int cases;
  int recovered;
  int avgCases7Day;
  int avgRecovered7Day;
  int avgDeath7Day;

  factory Overview.fromJson(Map<String, dynamic> json) => Overview(
        date: json["date"],
        death: json["death"],
        treating: json["treating"],
        cases: json["cases"],
        recovered: json["recovered"],
        avgCases7Day: json["avgCases7day"],
        avgRecovered7Day: json["avgRecovered7day"],
        avgDeath7Day: json["avgDeath7day"],
      );

  Map<String, dynamic> toJson() => {
        "date": date,
        "death": death,
        "treating": treating,
        "cases": cases,
        "recovered": recovered,
        "avgCases7day": avgCases7Day,
        "avgRecovered7day": avgRecovered7Day,
        "avgDeath7day": avgDeath7Day,
      };
}
