import 'package:covid_app/blocs/covid_bloc/covid_bloc.dart';
import 'package:covid_app/models/vietnam_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class VietNamPage extends StatefulWidget {
  const VietNamPage({Key? key}) : super(key: key);

  @override
  State<VietNamPage> createState() => _VietNamPageState();
}

class _VietNamPageState extends State<VietNamPage> {
  final CovidBloc _newsBloc = CovidBloc();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _newsBloc.add(GetVietNamCovidList());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('In Viet Nam'),
      ),
      body: _buildListCovid(),
    );
  }

  Widget _buildListCovid() {
    return SafeArea(
      child: Container(
        margin: EdgeInsets.all(8.0),
        child: BlocProvider(
          create: (_) => _newsBloc,
          child: BlocListener<CovidBloc, CovidState>(
            listener: (context, state) {
              if (state is CovidError) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(state.message!),
                  ),
                );
              }
            },
            child: BlocBuilder<CovidBloc, CovidState>(
              builder: (context, state) {
                if (state is CovidInitial) {
                  return _buildLoading();
                } else if (state is CovidLoading) {
                  return _buildLoading();
                } else if (state is CovidVietNamLoaded) {
                  return Column(
                    children: [
                      Container(
                        width: double.infinity,
                        height: 120,
                        margin: EdgeInsets.all(8.0),
                        child: Card(
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text(
                                    "Last Updated: ${state.covidModel.lastUpdatedAtApify}"),
                                Text(
                                    "Total infected: ${state.covidModel.infected}"),
                                Text(
                                    "Total recovered: ${state.covidModel.recovered}"),
                                Text(
                                    "Total treated: ${state.covidModel.treated}"),
                                Text("Total died: ${state.covidModel.died}"),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(child: _buildCard(context, state.covidModel))
                    ],
                  );
                } else if (state is CovidError) {
                  return Center(
                    child: Text(state.message!),
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildCard(BuildContext context, VietNamModel model) {
    return ListView.builder(
      itemCount: model.locations.length,
      itemBuilder: (context, index) {
        return Container(
          margin: EdgeInsets.all(4.0),
          child: Card(
            child: Container(
              padding: EdgeInsets.all(8),
              child: Column(
                children: <Widget>[
                  Text(
                    "Locations: ${model.locations[index].name}",
                    style: TextStyle(color: Colors.blue),
                  ),
                  Text("Total Cases: ${model.locations[index].cases}"),
                  Text(
                      "Total Cases Today: ${model.locations[index].casesToday}"),
                  Text("Total Recovered: ${model.locations[index].treating}"),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildLoading() => Center(child: CircularProgressIndicator());
}
