import 'package:bloc/bloc.dart';
import 'package:covid_app/models/vietnam_model.dart';
import 'package:equatable/equatable.dart';

import '../../models/covid_model.dart';
import '../../resources/api_repository.dart';

part 'covid_event.dart';
part 'covid_state.dart';

class CovidBloc extends Bloc<CovidEvent, CovidState> {
  CovidBloc() : super(CovidInitial()) {
    final ApiRepository _apiRepository = ApiRepository();

    on<GetCovidList>((event, emit) async {
      // TODO: implement event handler
      try {
        emit(CovidLoading());
        final mList = await _apiRepository.fetchCovidList();
        emit(CovidLoaded(mList!));
      } on NetworkError {
        emit(CovidError("Failed to fetch data. is your device online?"));
      }
    });

    on<GetVietNamCovidList>(
      (event, emit) async {
        try {
          emit(CovidLoading());
          final mList = await _apiRepository.fetchVietNamCovidList();
          emit(CovidVietNamLoaded(mList!));
        } on NetworkError {
          emit(CovidError("Failed to fetch data. is your device online?"));
        }
      },
    );
  }
}
